package org.example.model;

import lombok.*;
import org.apache.poi.ss.usermodel.CellType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 目前只支援字串、數字、日期處理
 * cr: 欄位橫軸名稱 A欄就填A, AZ欄就填AZ
 * cellType: 該欄位型別 字串請填CellType.STRING, 數字、日期填CellType.NUMERIC
 * value: 篩選值
 * @author liyanting
 */
@Data
@AllArgsConstructor
@Builder
public class CompareValue<T> {
    private String cr;
    private CellType cellType;
    private T value;

    public static Date toDate(String date, String pattern) {
        return getDate(pattern, date);
    }

    public static Date toDate(String date) {
        return getDate("yyyy-MM-dd", date);
    }

    private static Date getDate(String pattern, String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}