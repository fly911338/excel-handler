package org.example.config;

import lombok.Builder;
import lombok.Data;

/**
 * path: excel文件路徑
 * sheetName: excel工作表名稱(目前只支援單一工作表解析)
 * @author liyanting
 */
@Data
@Builder
public class ExcelConfig {
    /** 文件路徑 */
    private String path;
    private String sheetName;
}
