package org.example.service;

/**
 * @author liyanting
 */
public interface ParseExcel<T> {

    /**
     * 需實作解析excel的方式
     * @return 依需求指定回傳值
     */
    T parse();

    /**
     * 需實作關閉workbook得方法
     */
    void close();
}
