package org.example.service;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.example.model.CompareValue;

import java.util.Date;
import java.util.function.Predicate;

/**
 * 實作一些常用方法供子類客製條件時使用
 * @author liyanting
 */
public abstract class AbstractCondition {

    /**
     * 依照各需求實作所需的過濾資料方式
     * @return
     */
    public abstract Predicate<Row> filter();

    public Predicate<Row> or(Predicate<Row> predicate, Predicate<Row> predicate2) {
        return predicate.or(predicate2);
    }

    public Predicate<Row> and(Predicate<Row> predicate, Predicate<Row> predicate2) {
        return predicate.and(predicate2);
    }

    public Predicate<Row> stringShouldEqual(CompareValue<String> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isEqual(cellAddress.getStringCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> stringShouldNotEqual(CompareValue<String> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isNotEqual(cellAddress.getStringCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> numberShouldEqual(CompareValue<Double> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isEqual(cellAddress.getNumericCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> numberShouldNotEqual(CompareValue<Double> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isNotEqual(cellAddress.getNumericCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> numberShouldGreatThan(CompareValue<Double> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isGreatThenCompareValue(cellAddress.getNumericCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> numberShouldSmallThan(CompareValue<Double> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isSmallThenCompareValue(cellAddress.getNumericCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> dateShouldEqual(CompareValue<Date> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isEqual(cellAddress.getDateCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> dateShouldGreatThen(CompareValue<Date> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isDateGreatThenCompareValue(cellAddress.getDateCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> dateShouldGreatThenOrEqual(CompareValue<Date> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isDateGreatThenOrEqualCompareValue(cellAddress.getDateCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> dateShouldSmallThen(CompareValue<Date> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isDateSmallThenCompareValue(cellAddress.getDateCellValue(), compareValue.getValue());
        };
    }

    public Predicate<Row> dateShouldSmallThenOrEqual(CompareValue<Date> compareValue) {
        return (r) -> {
            Cell cellAddress = getCellAddress(r, compareValue.getCr());
            return cellVerify(cellAddress, compareValue.getCellType()) &&
                    isDateSmallThenOrEqualCompareValue(cellAddress.getDateCellValue(), compareValue.getValue());
        };
    }

    private boolean isGreatThenCompareValue(Double cellValue, Double compareValue) {
        return cellValue > compareValue;
    }

    private boolean isGreatThenOrEqualCompareValue(Double cellValue, Double compareValue) {
        return cellValue >= compareValue;
    }

    private boolean isSmallThenCompareValue(Double cellValue, Double compareValue) {
        return cellValue < compareValue;
    }


    private boolean isDateEqualCompareValue(Date cellValue, Date compareValue) {
        return cellValue.equals(compareValue);
    }

    private boolean isDateGreatThenCompareValue(Date cellValue, Date compareValue) {
        return cellValue.compareTo(compareValue) > 0;
    }

    private boolean isDateGreatThenOrEqualCompareValue(Date cellValue, Date compareValue) {
        return cellValue.compareTo(compareValue) >= 0;
    }

    private boolean isDateSmallThenOrEqualCompareValue(Date cellValue, Date compareValue) {
        return cellValue.compareTo(compareValue) <= 0;
    }

    private boolean isDateSmallThenCompareValue(Date cellValue, Date compareValue) {
        return cellValue.compareTo(compareValue) < 0;
    }


    private boolean isEqual(String cellValue, String compareValue) {
        return cellValue.equals(compareValue);
    }

    private boolean isEqual(Double cellValue, Double compareValue) {
        return cellValue.equals(compareValue);
    }

    private boolean isEqual(Date cellValue, Date compareValue) {
        return cellValue.equals(compareValue);
    }

    private boolean isNotEqual(String cellValue, String value) {
        return !cellValue.equals(value);
    }

    private boolean isNotEqual(Double cellValue, Double compareValue) {
        return !cellValue.equals(compareValue);
    }


    /**
     * 檢驗欄位是否為null及欄位行別是否與要比對的資料型別一致
     * @param cell 欄位
     * @param cellType 欄位型別
     * @return
     */
    private boolean cellVerify(Cell cell, CellType cellType) {
        return isNotNull(cell) &&
                cellTypeIsCorrect(cell, cellType);
    }

    private boolean cellTypeIsCorrect(Cell cellAddress, CellType cellType) {
        return cellAddress.getCellType() == cellType;
    }

    private boolean isNotNull(Cell cellAddress) {
        return cellAddress != null;
    }

    /**
     * 依據欄位座標取得找出指定欄位
     * @param row
     * @param reference 橫軸欄位座標名稱
     * @return 欄位
     */
    public Cell getCellAddress(Row row, String reference) {
        CellReference cr = new CellReference(reference + row.getRowNum());
        return row.getCell(cr.getCol());
    }
}
