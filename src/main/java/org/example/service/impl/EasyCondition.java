package org.example.service.impl;

import org.apache.poi.ss.usermodel.Row;
import org.example.model.CompareValue;
import org.example.service.AbstractCondition;

import java.util.function.Predicate;

/**
 * 簡易設定條件
 *
 * @author liyanting
 */
public class EasyCondition<T> extends AbstractCondition {

    CompareValue<T> value;

    public EasyCondition(CompareValue<T> value) {
        this.value = value;
    }

    /**
     * 實作過濾條件
     * 比對條件為
     *
     * @return 回傳使否符合條件
     */
    @Override
    public Predicate<Row> filter() {
        return null;
    }




}