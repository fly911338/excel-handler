package org.example.service.impl;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.example.config.ExcelConfig;
import org.example.service.AbstractCondition;
import org.example.service.ExcelConditionHelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.example.config.Const.AND;
import static org.example.config.Const.OR;

public class ExcelConditionHelperImpl implements ExcelConditionHelper {

    private Workbook wb;
    private ExcelConfig config;
    /**
     * 預設不過濾資料
     */
    private Predicate<Row> filterCondition = (row) -> true;

    public ExcelConditionHelperImpl(ExcelConfig config) {
        this.config = config;
        init();
    }

    @Override
    public void init() {
        try (FileInputStream fis = new FileInputStream(config.getPath())) {
            wb = WorkbookFactory.create(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addCondition(String operator, AbstractCondition condition) {
        switch (operator) {
            case AND:
                and(condition);
                break;
            case OR:
                or(condition);
                break;
            default:
                throw new RuntimeException("not found operator: " + operator);
        }
    }

    private void and(AbstractCondition condition) {
        filterCondition = filterCondition.and(condition.filter());
    }

    private void or(AbstractCondition condition) {
        filterCondition = filterCondition.or(condition.filter());
    }

    @Override
    public List<Row> getResult() {
        Sheet sheet = wb.getSheet(config.getSheetName());
        Stream<Row> stream = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(sheet.iterator(), Spliterator.ORDERED),
                false);
        return stream
                .filter(filterCondition)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getSpecificColumn(String cr) {
        List<Row> result = getResult();
        List<String> stringList = result.stream().map(rowToString(cr)).collect(Collectors.toList());
        return stringList;
    }

    private Function<Row, String> rowToString(String cr) {
        return row -> {
            Cell cell = getCell(cr, row);
            return getValue(cell);
        };
    }

    private String getValue(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case FORMULA:
            case NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case ERROR:
                throw new RuntimeException("");
            case BLANK:
            case _NONE:
            default:
                return "";
        }
    }

    private Cell getCell(String cr, Row row) {
        CellReference cellReference = new CellReference(cr);
        return row.getCell(cellReference.getCol() + 0);
    }

    @Override
    public void close() {
        try {
            wb.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
