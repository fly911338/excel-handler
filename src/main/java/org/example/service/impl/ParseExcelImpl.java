package org.example.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.example.config.ExcelConfig;
import org.example.service.AbstractCondition;
import org.example.service.ParseExcel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


/**
 * 實作excel解析方式
 * @author liyanting
 */
@Slf4j
public class ParseExcelImpl implements ParseExcel<List<Row>> {
    private Workbook wb;
    private AbstractCondition condition = null;
    private ExcelConfig config;
    private InputStream inp;

    public ParseExcelImpl(ExcelConfig config, AbstractCondition condition) {
        this.config = config;
        this.condition = condition;
        initWorkbook();
    }

    public ParseExcelImpl(ExcelConfig config) {
        this.config = config;
        initWorkbook();
    }

    private void initWorkbook() {
        try {
            inp = new FileInputStream(config.getPath());
            wb = WorkbookFactory.create(inp);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Row> parse() {
        Sheet sheet = wb.getSheet(config.getSheetName());
        Stream<Row> stream = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(sheet.iterator(), Spliterator.ORDERED),
                false);

        return getRows(stream);
    }

    private List<Row> getRows(Stream<Row> stream) {
        if (condition != null) {
//            stream = stream.filter(condition::filter);
        }
        return stream.collect(Collectors.toList());
    }

    @Override
    public void close() {
        try {
            wb.close();
            inp.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
