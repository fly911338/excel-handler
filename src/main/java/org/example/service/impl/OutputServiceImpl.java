package org.example.service.impl;

import org.apache.poi.ss.usermodel.*;
import org.example.config.ExcelConfig;
import org.example.service.OutputService;

import java.io.*;
import java.util.List;

/**
 * 在原文件最後一欄直接寫入結果
 *
 * @author liyanting
 */
public class OutputServiceImpl implements OutputService {

    private List<Row> rows;
    private Workbook wb;
    private ExcelConfig config;
    private InputStream inp;
    private Integer lastCellNum;


    public OutputServiceImpl(ExcelConfig config, List<Row> rows) {
        this.config = config;
        this.rows = rows;
        initWorkbook();
    }

    private void initWorkbook() {
        try {
            inp = new FileInputStream(config.getPath());
            wb = WorkbookFactory.create(inp);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void output() {
        try {
            FileOutputStream outputStream = new FileOutputStream(config.getPath());
            Sheet sheet = wb.getSheet(config.getSheetName());

            lastCellNum = findLastCellNum(sheet);

            for (Row row : sheet) {
                for (Row r : rows) {
                    if (r.getRowNum() == row.getRowNum()) {
                        Cell cell = row.createCell(lastCellNum);
                        cell.setCellValue("符合條件");
                    }
                }

            }
            wb.write(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            wb.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 找出欄位最後一欄
     *
     * @param sheet
     * @return
     */
    private Integer findLastCellNum(Sheet sheet) {
        int num = 0;
        for (Row row : sheet) {
            num = Math.max(num, row.getLastCellNum());
        }
        return num;
    }
}
