package org.example.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.example.model.CompareValue;
import org.example.service.AbstractCondition;
import org.example.service.ParseExcel;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.apache.poi.ss.usermodel.CellType.STRING;

/**
 * 解析excel取得特定欄位當作條件
 *
 * @author liyanting
 */
@Slf4j
public class ExcelSpecificColumnCondition extends AbstractCondition implements ParseExcel<Predicate<Row>> {

    private Workbook wb;
    private String column;

    public ExcelSpecificColumnCondition(String path, String column) {
        this.column = column;
        try (FileInputStream inp = new FileInputStream(path)) {
            wb = WorkbookFactory.create(inp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Predicate<Row> filter() {
        try {
            return parse();
        } finally {
            close();
        }
    }

    private Function<Row, Predicate<Row>> findValue() {
        return i -> {
            Cell cell = i.getCell(0);
            String batchCode = cell.getStringCellValue();
            CompareValue<String> a1 = new CompareValue<>(column, STRING, batchCode);
            return stringShouldEqual(a1);
        };
    }

    /**
     * 解析Ａ欄欄位資料將其所以字串都納入比對字串條件，只要任一相符就有寫入
     * skip(1)為標頭不用處理
     * @return Predicate<Row>
     */
    @Override
    public Predicate<Row> parse() {
        Sheet sheet = wb.getSheetAt(0);
        Stream<Row> stream = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(sheet.iterator(), Spliterator.ORDERED),
                false);

        return stream
                .skip(1)
                .map(findValue())
                .reduce(Predicate::or)
                .orElseThrow(() -> new RuntimeException("parse發生錯誤"));
    }

    @Override
    public void close() {
        try {
            wb.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
