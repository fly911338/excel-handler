package org.example.service;

public interface OutputService {

    void output();

    void close();
}
