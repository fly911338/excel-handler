package org.example.service;


import org.apache.poi.ss.usermodel.Row;

import java.util.List;

/**
 * @author liyanting
 */
public interface ExcelConditionHelper {

    void init();

    /**
     * 實作關閉excel方法
     */
    void close();

    /**
     * 依需求加入客制的條件
     * @param condition
     */
    void addCondition(String operator, AbstractCondition condition);

    /**
     * 取得Row物件
     * @return
     */
    List<Row> getResult();

    /**
     * 只取得特定欄位
     * @param cr
     * @return
     */
    List<String> getSpecificColumn(String cr);


}
