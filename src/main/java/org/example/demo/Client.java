package org.example.demo;

import org.example.config.ExcelConfig;
import org.example.model.CompareValue;
import org.example.service.AbstractCondition;
import org.example.service.impl.EasyCondition;
import org.example.service.impl.ExcelConditionHelperImpl;

import java.util.List;

import static org.apache.poi.ss.usermodel.CellType.STRING;
import static org.example.config.Const.AND;

public class Client {
    public static void main(String[] args) {
        ExcelConfig config = ExcelConfig.builder()
                .path("/Users/liyanting/Downloads/贊助明細.xlsx")
                .sheetName("贊助明細")
                .build();

        ExcelConditionHelperImpl impl = new ExcelConditionHelperImpl(config);

        CompareValue<String> shouldEqual1 = new CompareValue<>("A", STRING, "傑爸廚房");
        CompareValue<String> shouldNotEqual1 = new CompareValue<>("A", STRING, "小殭屍棺材板");
        CompareValue<String> shouldEqual2 = new CompareValue<>("A", STRING, "泰好喝");

        AbstractCondition condition = new EasyCondition(shouldEqual1);

        impl.addCondition(AND, condition);


//        String path = "/Users/liyanting/Downloads/a.xlsx";
//        ExcelSpecificColumnCondition a2 = new ExcelSpecificColumnCondition(path, "A");




        List<String> a1 = impl.getSpecificColumn("A");
        System.out.println(a1);
        impl.close();

    }
}
